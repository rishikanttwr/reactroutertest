import React, { Component } from 'react'
import PropTypes from 'prop-types'

export default class HomeTableRow extends Component {
  static propTypes = {
    prop: PropTypes.string
  }

  render() {
    const {expense} = this.props;
    return <tr>
      <td><input className="form-check-input" type="checkbox" id="autoSizingCheck2" value={expense.id} onInput={this.props.onSelectListner}/></td>
        <td>{expense.id}</td>
        <td>{expense.date}</td>
        <td>{expense.from}</td>
        <td>{expense.to}</td>
        <td>{expense.Amount}</td>
      <td>{expense.purpose}</td>
      </tr>;
  }
}
