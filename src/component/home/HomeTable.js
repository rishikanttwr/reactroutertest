import React, { Component } from "react";
import PropTypes from "prop-types";
import HomeTableRow from "./HomeTableRow";

export default class HomeTable extends Component {
  static propTypes = {
    prop: PropTypes.array
  };

  render() {
    const {expenses } = this.props;
    return <table className="table table-striped">
        <thead>
          <tr>
          <th scope="col">Select</th>
            <th scope="col">#</th>
            <th scope="col">Date</th>
            <th scope="col">From</th>
            <th scope="col">To</th>
            <th scope="col">Amount</th>
            <th scope="col">abc</th>

          </tr>
        </thead>
        <tbody>
          {expenses.map(expense => (
          <HomeTableRow key={expense.id} expense={expense} onSelectListner={this.props.onSelectListner}/>
          ))}
        </tbody>
      </table>;
  }
}
