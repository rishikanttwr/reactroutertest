import React, { Component } from "react";
import { Link } from "react-router-dom";
export default class Header extends Component {
  render() {
    return <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <div className="collapse navbar-collapse" id="navbarNav">
          <ul className="navbar-nav">
            <li className="nav-item active">
              <div>
                <Link to="/" className="nav-link">
                  Home
                </Link>
              </div>
            </li>
            <li className="nav-item">
              <div>
                <Link className="nav-link" to="AddRecord">
                  Add Record
                </Link>
              </div>
            </li>
            <li className="nav-item">
              <div>
                <Link className="nav-link" to="RemoveRecord">
                  Remove Record
                </Link>
              </div>
            </li>
            <li className="nav-item">
              <div>
                <Link className="nav-link" to="About">
                  About
                </Link>
              </div>
            </li>
          </ul>
        </div>
      </nav>;
  }
}
