import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import HomeTable from "./home/HomeTable";
import { bindActionCreators } from "redux";
import * as expenseActions from "../actions/expenseActions";
export class Home extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      expenses: Object.assign({}, props.expenses),
      errors: {},
      saving: false,
      selctedId: "0"
    };
    this.onClickHandler = this.onClickHandler.bind(this);
  }
  static propTypes = { prop: PropTypes.array };

  render() {
    const { expenses } = this.props;
    const btnClass =
      "btn btn-primary " +
      (this.state.selctedId !== "0" ? "active" : "disabled");
    console.log(btnClass);
    console.log("Home ===" + expenses.constructor);
    return (
      <div>
        <HomeTable expenses={expenses} onSelectListner={this.onSelectListner} />
        <button
          type="button"
          className={btnClass}
          onClick={this.onClickHandler}
        >
          Delete Record
        </button>
      </div>
    );
  }

  onClickHandler(event) {
    console.log(this.state.selctedId);
    event.preventDefault();
    this.props.actions
      .deletExpense(this.state.selctedId)
      .then(() => this.setState({ selctedId: "0" }))
      .catch(error => {
        this.setState({ saving: false });
      });
  }
  onSelectListner = event => {
    if (event.target.checked) this.setState({ selctedId: event.target.value });
    else this.setState({ selctedId: "0" });
  };
}

const mapStateToProps = state => ({
  expenses: state
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(expenseActions, dispatch)
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
