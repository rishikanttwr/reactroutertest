import React, { Component } from "react";
import "./App.css";
import Header from "./component/common/Header";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Home from "./component/Home";
import AddPage from "./component/AddPage";
import RemovePage from "./component/RemovePage";
import About from "./component/About";
import NotFound from "./component/NotFound";
// import SideBar from "./component/common/SideBar";

class App extends Component {
  render() {
    return <Router>
        <div className="App">
          <Header />
          <div>
            <Switch>
              <Route path="/" render={() => <Home />} exact={true} />
              <Route path="/AddRecord" render={() => <AddPage />} />
              <Route path="/RemoveRecord" render={() => <RemovePage />} />
              <Route path="/About" render={() => <About />} />
              <Route render={() => <NotFound />} />
            </Switch>
          </div>
        </div>
      </Router>;
  }
}

export default App;
