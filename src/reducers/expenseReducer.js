import * as types from '../actions/actionTypes';
// import ExpenseApi from '../api/mockHomeData';
const initialState = {
  expenses:[]
}

export default (state = initialState.expenses, action) => {
  switch (action.type) {
    case types.ADD_EXPENSE_SUCCESS:
      return { ...state, action };
    case types.DELETE_EXPENSE_SUCCESS:
      return [...state.filter(expense => expense.id !== action.id)];
      // return [...state.filter(expense => expense.id !== 1)];
    case types.UPDATE_EXPENSE_SUCCESS:
      return { ...state }
    case types.LOAD_EXPENSE_SUCCESS:
      return action.expenses;
  default:
    return state
  }
}
