import delay from './delay';

// This file mocks a web API by working with the hard-coded data below.
// It uses setTimeout to simulate the delay of an AJAX call.
// All calls return promises.
const expenses = [
  {
    id: "1",
    date: "05-07-2019",
    from: "Marathalli",
    to: "Home",
    Amount: "200",
    purpose: "work related"
  },
  {
    id: "2",
    date: "05-07-2019",
    from: "Marathalli",
    to: "JP Nagar",
    Amount: "300",
    purpose: "work related"
  },
  {
      id: "3",
      date: "05-07-2019",
      from: "Panathur",
      to: "Marathalli",
      Amount: "300",
      purpose: "work related"
  }
];

function replaceAll(str, find, replace) {
    return str.replace(new RegExp(find, 'g'), replace);
}

//This would be performed on the server in a real app. Just stubbing in.
const generateId = (expense) => {
    return replaceAll(expense.date, " ", "-");
};

class ExpenseApi {
    static getAllExpense() {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve(Object.assign([], expenses));
            }, delay);
        });
    }

    static saveExpense(expense) {
        expense = Object.assign({}, expense); // to avoid manipulating object passed in.
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                // Simulate server-side validation
                const minAmountLength = 1;
                if (expense.Amount.length < minAmountLength) {
                  reject(`Title must be at least ${minAmountLength} characters.`);
                }

                if (expense.id) {
                    const existingExpenseIndex = expenses.findIndex(a => a.id === expense.id);
                    expenses.splice(existingExpenseIndex, 1, expense);
                } else {
                    //Just simulating creation here.
                    //The server would generate ids and watchHref's for new courses in a real app.
                    //Cloning so copy returned is passed by value rather than by reference.
                    expense.id = generateId(expense);
                    // expense.watchHref = `http://www.pluralsight.com/courses/${expense.id}`;
                    expenses.push(expense);
                }

                resolve(expense);
            }, delay);
        });
    }

    static deleteExpense(expenseId) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                const indexOfExpenseToDelete = expenses.findIndex(expense => {
                    return expense.id === expenseId;
                });
                expenses.splice(indexOfExpenseToDelete, 1);
                resolve();
            }, delay);
        });
    }
}

export default ExpenseApi;
