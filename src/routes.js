import React from "react";
import { BrowserRouter as Router, Route} from "react-router-dom";
import App from "./App";
//import Home from "./component/Home";
import About from "./component/About";
import AddPage from "./component/AddPage";
import RemovePage from "./component/RemovePage";
//import NotFound from "./component/NotFound";

const routes = 
[
  {
    path: "/Home",
      component: App
  },
  {
    path: "/AddPage",
    component: AddPage
  },
  {
    path: "/RemovePage",
    component: RemovePage
  },
  {
    path: "/About",
    component: About
  }
  
];

const RouteWithSubRoutes = route => (
  <Route
    path={route.path}
    render={props => (
      // pass the sub-routes down to keep nesting
      <route.component {...props} routes={route.routes} />
    )}
  />
);
const RoutesComp = () => (
  <Router>
    <div>
      {routes.map((route, i) => <RouteWithSubRoutes key={i} {...route} />)}
    </div>
  </Router>
);
export default RoutesComp;
