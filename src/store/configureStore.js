import { createStore, applyMiddleware } from 'redux';
import expenseReducer from "../reducers/expenseReducer";
// import reduxImmutableStateInvariant from 'redux-immutable-state-invariant';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';

export default function configureStore(initialState) {
    return createStore(expenseReducer, initialState, applyMiddleware(thunk, createLogger()));
}
