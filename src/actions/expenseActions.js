import * as types from './actionTypes';
import expenseApi from '../api/mockHomeData'
import { beginAjaxCall, ajaxCallError } from './ajaxStatusActions';
export function loadExpenseSuccess(expenses) {
    return { type: types.LOAD_EXPENSE_SUCCESS,expenses };
}

export function createExpenseSuccess(expense) {
    return { type: types.ADD_EXPENSE_SUCCESS, expense };
}

export function updateExpenseSuccess(expense) {
    return { type: types.UPDATE_EXPENSE_SUCCESS, expense };
}

export function deleteExpenseSuccess(id) {
    return { type: types.DELETE_EXPENSE_SUCCESS ,id};
}

export function loadExpenses() {
    return function (dispatch) {
        dispatch(beginAjaxCall());
        return expenseApi
          .getAllExpense()
          .then(expenses => {
            dispatch(loadExpenseSuccess(expenses));
          })
          .catch(error => {
            throw error;
          });
    };
}

export function saveExpense(expense) {
    return function (dispatch, getState) {
        dispatch(beginAjaxCall());
        return expenseApi
          .saveExpense(expense)
          .then(expense => {
            expense.id ? dispatch(updateExpenseSuccess(expense)) : dispatch(createExpenseSuccess(expense));
          })
          .catch(error => {
            dispatch(ajaxCallError(error));
            throw error;
          });
    };
}

export function deletExpense(expenseId) {
    return function (dispatch, getState) {
        dispatch(beginAjaxCall());
        return expenseApi
            .deleteExpense(expenseId)
            .then(() => {
                dispatch(deleteExpenseSuccess(expenseId));
          })
          .catch(error => {
            dispatch(ajaxCallError(error));
            throw error;
          });
    };
}